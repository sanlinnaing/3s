package com.sanlin.swarmlogin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.cengalabs.flatui.FlatUI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import lu.thinds.android.RestService;

public class ForgetPassword extends ActionBarActivity {
    private final int APP_THEME = R.array.orange;
    private ArrayList<IDValue> quesList = new ArrayList<IDValue>();
    private String ques1Id = null, ques2Id = null;
    private Button btnReset;
    Handler mHandlerPostUserName = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            Log.d("mHandlerPostUserName", msg.obj.toString());
            try {
                JSONObject ret = new JSONObject((String) msg.obj);
                JSONArray retA = ret.getJSONArray("data");
                String retCount = retA.getJSONObject(0).getString("count(1)");
                Log.d("mHandlerPostUserName", retCount);
                if (retCount.equals("0")) {
                    txtLoginId.setError("This LoginID is not belong to a member. Please try another.");
                    btnReset.setEnabled(false);
                    txtAns1.setEnabled(false);
                    txtAns2.setEnabled(false);
                } else {
                    btnReset.setEnabled(true);
                    txtAns1.setEnabled(true);
                    txtAns2.setEnabled(true);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };
    private EditText txtAns1, txtAns2, txtLoginId;
    private Spinner spnQues1, spnQues2;
    Handler mHandlerGet = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try {
                JSONObject country_ques = new JSONObject((String) msg.obj);
                JSONArray questions = country_ques.getJSONArray("questions");
                initializeSpinnerQuestionsLists(questions);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    private String mode = null;
    private Context mContext;
    private ProgressDialog progressBar;
    private Intent newPasswd;
    Handler mHandlerCheckQandAPost = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try {
                boolean correct = true;
                JSONObject ret = new JSONObject((String) msg.obj);
                showToast(msg.obj.toString());
                if (!ret.getString("s1").equals("1")) {
                    txtAns1.setError("Answer1 not match with question!");
                    correct = false;
                }
                if (!ret.getString("s2").equals("1")) {
                    txtAns2.setError("Answer2 not match with question!");
                    correct = false;
                }
                if (correct) {
                    showToast("Answer 1 and 2 correct.");
                    newPasswd.putExtra("mode", mode);
                    newPasswd.putExtra("loginId", txtLoginId.getText().toString());
                    startActivity(newPasswd);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressBar.dismiss();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlatUI.initDefaultValues(this);

        // Default theme should be set before content view is added
        FlatUI.setDefaultTheme(APP_THEME);

        setContentView(R.layout.activity_forget_password);
        getSupportActionBar().setBackgroundDrawable(FlatUI.getActionBarDrawable(this, APP_THEME, false));
        getSupportActionBar().setBackgroundDrawable(FlatUI.getActionBarDrawable(this, APP_THEME, false));
        if(!Utilities.isOnline(this)){
            finish();
        }
        initialize();
        loadQuestion();
        addListener();
        validator();
    }

    private void validator() {
        txtAns1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && txtAns1.length() < 1) {
                    txtAns1.setError("It is required!");
                }
            }
        });
        txtAns2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && txtAns2.length() < 1) {
                    txtAns2.setError("It is required!");
                }
            }
        });
        txtLoginId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && txtLoginId.length() < 1) {
                    txtLoginId.setError("It is required!");
                } else
                    checkUserNameUsed(txtLoginId.getText().toString());
            }
        });
    }

    private void addListener() {
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtAns1.length() < 0)
                    txtAns1.setError("It is required.");
                else if (txtAns2.length() < 0) {
                    txtAns2.setError("It is required.");
                } else if (txtLoginId.length() < 0)
                    txtLoginId.setError("It is required.");
                else

                    checkQuesAndAns();

            }
        });

    }

    private void checkUserNameUsed(String username) {
        RestService getState = new RestService(mHandlerPostUserName, mContext, "http://23.92.53.174/testws/getdroplist.php", RestService.POST);
        getState.setEntity("{\"type\":3,\"username\":\"" + username + "\"}");
        getState.execute();
    }

    private void checkQuesAndAns() {
        String ans1 = txtAns1.getText().toString();
        String ans2 = txtAns2.getText().toString();
        String loginId = txtLoginId.getText().toString();
        mode = Utilities.checkMode(loginId);
        String entity = "{\"type\":1,\"qus1\":" + ques1Id + ",\"ans1\":\"" + ans1 + "\",\"qus2\":" + ques2Id + ",\"ans2\":\"" + ans2 + "\",\"mode\":" +
                mode + ",\"username\":\"" + loginId + "\"}";
        Log.d("checkQuesAndAns", entity);
        RestService checkQandA = new RestService(mHandlerCheckQandAPost, mContext, "http://23.92.53.174/testws/management.php", RestService.POST);
        checkQandA.setEntity(entity);
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Checking ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.setCancelable(false);
        progressBar.show();
        checkQandA.execute();
    }

    private void loadQuestion() {
        RestService getCountryQuestion = new RestService(mHandlerGet, this, "http://23.92.53.174/testws/SignupBefore.php", RestService.GET);
        getCountryQuestion.execute();
    }

    private void initialize() {
        mContext = this;
        newPasswd = new Intent(this, NewPasswordActivity.class);
        spnQues1 = (Spinner) findViewById(R.id.spn_fg_ques1);
        spnQues2 = (Spinner) findViewById(R.id.spn_fg_ques2);
        btnReset = (Button) findViewById(R.id.btn_reset);
        btnReset.setEnabled(false);
        txtAns1 = (EditText) findViewById(R.id.txt_fg_ans1);
        txtAns1.setEnabled(false);
        txtAns2 = (EditText) findViewById(R.id.txt_fg_ans2);
        txtAns2.setEnabled(false);
        txtLoginId = (EditText) findViewById(R.id.txt_fg_loginid);
    }

    private void initializeSpinnerQuestionsLists(JSONArray questions) {
        ArrayList<IDValue> q1List = new ArrayList<IDValue>();
        ArrayList<IDValue> q2List = new ArrayList<IDValue>();
        try {
            String quesid;
            String questionsName;
            JSONObject item = questions.getJSONObject(0);
            quesid = item.getString("questionsid");
            questionsName = item.getString("questionsname");
            q1List.add(0, new IDValue(quesid, questionsName));
            quesList.add(0, new IDValue(quesid, questionsName));
            for (int i = 1; i < questions.length(); i++) {
                item = questions.getJSONObject(i);
                quesid = item.getString("questionsid");
                questionsName = item.getString("questionsname");
                q1List.add(i, new IDValue(quesid, questionsName));
                q2List.add(i - 1, new IDValue(quesid, questionsName));
                quesList.add(i, new IDValue(quesid, questionsName)); // store questions for further use
            }
        } catch (JSONException e) {
            e.printStackTrace();
            q1List.add(0, new IDValue("0", "NO VALUE!"));
            q2List.add(0, new IDValue("0", "NO VALUE!"));
        }
        IDValueArrayAdapter question1Adapter = new IDValueArrayAdapter(this, android.R.layout.simple_spinner_item, q1List);
        question1Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnQues1.setAdapter(question1Adapter);
        spnQues1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ques1Id = "" + id;
                updateQues2List(ques1Id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        IDValueArrayAdapter question2Adapter = new IDValueArrayAdapter(this, android.R.layout.simple_spinner_item, q2List);
        question2Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnQues2.setAdapter(question2Adapter);
        spnQues2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ques2Id = "" + id;
                if (ques2Id.equals(ques1Id)) {
                    updateQues2List(ques1Id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void updateQues2List(String ques1id) {
        ArrayList<IDValue> itemq2list = new ArrayList<IDValue>();
        int j = 0;
        for (int i = 0; i < quesList.size(); i++) {
            if (quesList.get(i).getId().equals(ques1id)) {
                continue;
            }
            itemq2list.add(j, quesList.get(i));
            j++;
        }

        IDValueArrayAdapter question2Adapter = new IDValueArrayAdapter(this, android.R.layout.simple_spinner_item, itemq2list);
        question2Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnQues2.setAdapter(question2Adapter);
        spnQues2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ques2Id = "" + id;
                if (ques2Id.equals(ques1Id)) {
                    updateQues2List(ques1Id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void showToast(String msg) {
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        toast.show();
    }
}
