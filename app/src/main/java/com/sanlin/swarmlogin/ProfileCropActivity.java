package com.sanlin.swarmlogin;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.cengalabs.flatui.FlatUI;
import com.github.crystalpal.cropper.CropView;

import java.io.File;

public class ProfileCropActivity extends ActionBarActivity {
    private final int APP_THEME = R.array.orange;
    private final int REQUEST_IMAGE_SELECTOR = 123;
    private Button btnCrop;
    private CropView cropView;
    private File mFile;
    private BitmapDrawable mBitmapDrawable;

    private void cropImage() {
        mBitmapDrawable = (BitmapDrawable) Drawable.createFromPath(mFile.getAbsolutePath());
        cropView = (CropView) findViewById(R.id.crop_preview);
        cropView.setImageDrawable(mBitmapDrawable);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlatUI.initDefaultValues(this);

        // Default theme should be set before content view is added
        FlatUI.setDefaultTheme(APP_THEME);

        setContentView(R.layout.activity_profile_crop);
        getSupportActionBar().setBackgroundDrawable(FlatUI.getActionBarDrawable(this, APP_THEME, false));
        getSupportActionBar().setBackgroundDrawable(FlatUI.getActionBarDrawable(this, APP_THEME, false));
        initialize();
        addListener();
        loadImage();
    }

    private void loadImage() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        this.startActivityForResult(galleryIntent, REQUEST_IMAGE_SELECTOR);
    }

    private void addListener() {
        btnCrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byte[] mByteArray = cropView.getCroppedImage(mBitmapDrawable,
                        cropView.getWidth(), cropView.getHeight());
                String encodedString = Base64.encodeToString(mByteArray, 0);
                Log.d("CropImage", "length = "+ encodedString.length()+" : : "+encodedString);
                Intent ret=new Intent();
                ret.putExtra("croppedPhoto",encodedString);
                setResult(1,ret);
                finish();
            }
        });
    }

    private void initialize() {
        cropView = new CropView(this);
        btnCrop = (Button) findViewById(R.id.btnCrop);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_IMAGE_SELECTOR:
                if (resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(data.getData(), filePathColumn, null, null, null);
                    if (cursor == null || cursor.getCount() < 1) {
                        mFile = null;
                        break;
                    }
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    if (columnIndex < 0) { // no column index
                        mFile = null;
                        break;
                    }
                    mFile = new File(cursor.getString(columnIndex));
                    cursor.close();
                } else {
                    mFile = null;
                }
                break;
        }
        if (mFile != null) {
            Log.d("MyFilePath", mFile.getAbsolutePath());
            cropImage();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
