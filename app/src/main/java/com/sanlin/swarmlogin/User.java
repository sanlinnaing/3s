package com.sanlin.swarmlogin;

/**
 * Created by San Lin Naing on 8/26/2015.
 */
public class User {
    String SignMode, p_UserName, p_EmailAddress, p_MobileNumber, p_Country, p_State,
            p_City, p_HomeTown, p_FullName, p_DOB, p_NickName, p_BillingAddress, p_ProfilePicture,
            p_Plain_Password_text, p_EncryptionPasswrod, p_qus1, p_ans1, p_qus2, p_ans2;

    public String getSignMode() {
        return SignMode;
    }

    public void setSignMode(String signMode) {
        SignMode = signMode;
    }

    public String getP_UserName() {
        return p_UserName;
    }

    public void setP_UserName(String p_UserName) {
        this.p_UserName = p_UserName;
    }

    public String getP_EmailAddress() {
        return p_EmailAddress;
    }

    public void setP_EmailAddress(String p_EmailAddress) {
        this.p_EmailAddress = p_EmailAddress;
    }

    public String getP_MobileNumber() {
        return p_MobileNumber;
    }

    public void setP_MobileNumber(String p_MobileNumber) {
        this.p_MobileNumber = p_MobileNumber;
    }

    public String getP_Country() {
        return p_Country;
    }

    public void setP_Country(String p_Country) {
        this.p_Country = p_Country;
    }

    public String getP_State() {
        return p_State;
    }

    public void setP_State(String p_State) {
        this.p_State = p_State;
    }

    public String getP_City() {
        return p_City;
    }

    public void setP_City(String p_City) {
        this.p_City = p_City;
    }

    public String getP_HomeTown() {
        return p_HomeTown;
    }

    public void setP_HomeTown(String p_HomeTown) {
        this.p_HomeTown = p_HomeTown;
    }

    public String getP_FullName() {
        return p_FullName;
    }

    public void setP_FullName(String p_FullName) {
        this.p_FullName = p_FullName;
    }

    public String getP_DOB() {
        return p_DOB;
    }

    public void setP_DOB(String p_DOB) {
        this.p_DOB = p_DOB;
    }

    public String getP_NickName() {
        return p_NickName;
    }

    public void setP_NickName(String p_NickName) {
        this.p_NickName = p_NickName;
    }

    public String getP_BillingAddress() {
        return p_BillingAddress;
    }

    public void setP_BillingAddress(String p_BillingAddress) {
        this.p_BillingAddress = p_BillingAddress;
    }

    public String getP_ProfilePicture() {
        return p_ProfilePicture;
    }

    public void setP_ProfilePicture(String p_ProfilePicture) {
        this.p_ProfilePicture = p_ProfilePicture;
    }

    public String getP_Plain_Password_text() {
        return p_Plain_Password_text;
    }

    public void setP_Plain_Password_text(String p_Plain_Password_text) {
        this.p_Plain_Password_text = p_Plain_Password_text;
    }

    public String getP_EncryptionPasswrod() {
        return p_EncryptionPasswrod;
    }

    public void setP_EncryptionPasswrod(String p_EncryptionPasswrod) {
        this.p_EncryptionPasswrod = p_EncryptionPasswrod;
    }

    public String getP_qus1() {
        return p_qus1;
    }

    public void setP_qus1(String p_qus1) {
        this.p_qus1 = p_qus1;
    }

    public String getP_ans1() {
        return p_ans1;
    }

    public void setP_ans1(String p_ans1) {
        this.p_ans1 = p_ans1;
    }

    public String getP_qus2() {
        return p_qus2;
    }

    public void setP_qus2(String p_qus2) {
        this.p_qus2 = p_qus2;
    }

    public String getP_ans2() {
        return p_ans2;
    }

    public void setP_ans2(String p_ans2) {
        this.p_ans2 = p_ans2;
    }
}
