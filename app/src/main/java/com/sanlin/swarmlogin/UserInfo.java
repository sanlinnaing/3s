package com.sanlin.swarmlogin;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by San Lin Naing on 8/23/2015.
 */
public class UserInfo implements Parcelable {
    private String txtFullName, txtUserName, txtPassword, txtConfirmPasswd, txtEmail, txtDob, txtPhone;

    public UserInfo(String txtFullName, String txtUserName, String txtPassword, String txtConfirmPasswd, String txtEmail, String txtDob, String txtPhone) {
        this.txtFullName = txtFullName;
        this.txtUserName = txtUserName;
        this.txtPassword = txtPassword;
        this.txtConfirmPasswd = txtConfirmPasswd;
        this.txtEmail = txtEmail;
        this.txtDob = txtDob;
        this.txtPhone = txtPhone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(txtFullName);
        dest.writeString(txtUserName);
        dest.writeString(txtPassword);
        dest.writeString(txtConfirmPasswd);
        dest.writeString(txtEmail);
        dest.writeString(txtDob);
        dest.writeString(txtPhone);

    }
    private UserInfo(Parcel in){
        this.txtFullName=in.readString();
        this.txtUserName=in.readString();
        this.txtPassword=in.readString();
        this.txtConfirmPasswd=in.readString();
        this.txtEmail=in.readString();
        this.txtDob=in.readString();
        this.txtPhone=in.readString();
    }
    public static final Parcelable.Creator<UserInfo> CREATOR=new Parcelable.ClassLoaderCreator<UserInfo>(){

        @Override
        public UserInfo createFromParcel(Parcel source, ClassLoader loader) {
            return new UserInfo(source);
        }

        @Override
        public UserInfo createFromParcel(Parcel source) {
            return new UserInfo(source);
        }

        @Override
        public UserInfo[] newArray(int size) {
            return new UserInfo[size];
        }


    };

    @Override
    public String toString() {
        return "UserInfo{" +
                "txtFullName='" + txtFullName + '\'' +
                ", txtUserName='" + txtUserName + '\'' +
                ", txtPassword='" + txtPassword + '\'' +
                ", txtConfirmPasswd='" + txtConfirmPasswd + '\'' +
                ", txtEmail='" + txtEmail + '\'' +
                ", txtDob='" + txtDob + '\'' +
                ", txtPhone='" + txtPhone + '\'' +
                '}';
    }
}
