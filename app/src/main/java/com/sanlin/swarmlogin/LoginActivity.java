package com.sanlin.swarmlogin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cengalabs.flatui.FlatUI;
import com.facebook.appevents.AppEventsLogger;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

import lu.thinds.android.RestService;

public class LoginActivity extends ActionBarActivity {
    private final int APP_THEME = R.array.orange;
    private ProgressDialog progressBar = null;
    private String loginID = "";
    private Intent home;
    private SharedPreferences appData;
    private EditText txtLoginId = null;
    private EditText txtPasswd = null;
    private final Handler mHandlerPost = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            progressBar.setProgress(50);
            String status = "0", message = "";
            try {
                JSONObject receive = new JSONObject((String) msg.obj);
                status = receive.get("status").toString();
                message = receive.get("msg").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (status.equals("1")) {
                home.putExtra("username", loginID);
                SharedPreferences.Editor editor = appData.edit();
                editor.putBoolean("login_status", true);
                editor.commit();
                startActivity(home);

                finish();
            } else {
                toastShow("Login ID and password mismatched.");
                txtPasswd.setText("");
            }

            progressBar.dismiss();
        }
    };

    private boolean first = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlatUI.initDefaultValues(this);

        // Default theme should be set before content view is added
        FlatUI.setDefaultTheme(APP_THEME);
        setContentView(R.layout.activity_login);
        // Getting action bar background and applying it
        getSupportActionBar().setBackgroundDrawable(FlatUI.getActionBarDrawable(this, APP_THEME, false));
        getSupportActionBar().setBackgroundDrawable(FlatUI.getActionBarDrawable(this, APP_THEME, false));

        appData = PreferenceManager.getDefaultSharedPreferences(this);
        final Intent registration = new Intent(this, RegistrationActivity.class);
        home = new Intent(this, MemberHomeActivity.class);
        txtLoginId = (EditText) findViewById(R.id.txt_login);
        txtPasswd = (EditText) findViewById(R.id.txt_loginpasswd);

        txtLoginId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (txtLoginId.getText().toString().length() <= 0) {
                    txtLoginId.setError(getString(R.string.err_loginid_required));
                }
            }
        });
        final Intent forgetPasswd = new Intent(this, ForgetPassword.class);
        TextView tvForgetPasswd = (TextView) findViewById(R.id.tv_forget_passwd);
        tvForgetPasswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(forgetPasswd);
            }
        });
        Button btnSignUp = (Button) findViewById(R.id.btn_sign_up);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View view) {
                                             startActivity(registration);
                                         }
                                     }

        );
        Button btnLogin = (Button) findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (txtLoginId.getText().length() <= 0) {
                                                txtLoginId.setError(getString(R.string.err_loginid_required));
                                                txtLoginId.requestFocus();
                                            } else if (txtPasswd.getText().length() < 6) {
                                                txtPasswd.setError((getString(R.string.err_password_length)));
                                                txtPasswd.requestFocus();
                                            } else
                                                checkLogin();
                                        }
                                    }

        );
    }

    private void checkLogin() {

        loginID = txtLoginId.getText().toString();
        String password = txtPasswd.getText().toString();
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Checking Network ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
        if (!Utilities.isOnline(this)) {

            progressBar.dismiss();
            return;

        }
        progressBar.setMessage("Logging in ...");
        if (checkLogin(loginID, password)) {
            //toastShow("Checked login");
        }
    }

    private boolean checkLogin(String loginId, String password) {

        String mode = checkMode(loginId);
        //toastShow("mode = " + mode);
        RestService loginPost = new RestService(mHandlerPost, this, "http://23.92.53.174/testws/logincheck.php", RestService.POST); //Create new rest service for post
        loginPost.addParam("mode", mode); //Add params to request
        loginPost.addParam("p_UserID", loginId); //Format for a typical form encoded nested attribute
        loginPost.addParam("p_Password", stringToMD5(password));
        loginPost.execute();
        return true;
    }

    private String checkMode(String loginId) {
        String phonePattern = "[0-9]{10}";
        String email = "^[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$";
        Pattern ptnPhone = Pattern.compile(phonePattern);
        Pattern ptnEmail = Pattern.compile(email);
        if (ptnPhone.matcher(loginId).find())
            return "1";
        else if (ptnEmail.matcher(loginId).find())
            return "2";
        else
            return "3";
    }

    private String stringToMD5(String plaintext) {
        MessageDigest m = null;
        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        m.reset();
        m.update(plaintext.getBytes());
        byte[] digest = m.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        String md5Text = bigInt.toString(16);
// Now we need to zero pad it if you actually want the full 32 chars.
        while (md5Text.length() < 32) {
            md5Text = "0" + md5Text;
        }
        //toastShow(md5Text);
        return md5Text;
    }

    private void toastShow(String msg) {
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        toast.show();
    }
}
