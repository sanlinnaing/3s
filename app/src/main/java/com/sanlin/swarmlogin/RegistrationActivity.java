package com.sanlin.swarmlogin;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.cengalabs.flatui.FlatUI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Pattern;

import lu.thinds.android.RestService;

public class RegistrationActivity extends ActionBarActivity {
    private final int APP_THEME = R.array.orange;
    private final int REQUEST_PROFILE = 16;
    Handler mHandlerPostState = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try {
                JSONObject state = new JSONObject((String) msg.obj);
                JSONArray stateArray = state.getJSONArray("data");
                updateStateSpinnerList(stateArray);

            } catch (JSONException e) {
                updateStateSpinnerList(null);
                e.printStackTrace();
            }
        }

    };
    Handler mHandlerPostCity = new Handler() {


        @Override
        public void handleMessage(Message msg) {
            Log.d("mHandlerPostCity", msg.obj.toString());
            try {
                JSONObject city = new JSONObject((String) msg.obj);
                JSONArray cityArray = city.getJSONArray("data");
                updateCitySpinnerList(cityArray);
            } catch (JSONException e) {
                updateCitySpinnerList(null);
                e.printStackTrace();
            }
        }
    };
    Handler mHandlerPostPhone = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            Log.d("mHandlerPostPhone", msg.obj.toString());
            try {
                JSONObject ret = new JSONObject((String) msg.obj);
                JSONArray retA = ret.getJSONArray("data");
                String retCount = retA.getJSONObject(0).getString("count(1)");
                Log.d("mHandlerPostPhone", retCount);
                if (!retCount.equals("0")) {
                    txtPhone.setError("This phone number is already used. Please use another phone number.");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };
    Handler mHandlerPostEmail = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            Log.d("mHandlerPostPhone", msg.obj.toString());
            try {
                JSONObject ret = new JSONObject((String) msg.obj);
                JSONArray retA = ret.getJSONArray("data");
                String retCount = retA.getJSONObject(0).getString("count(1)");
                Log.d("mHandlerPostPhone", retCount);
                if (!retCount.equals("0")) {
                    txtEmail.setError("This email is already used. Please use another email.");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };
    Handler mHandlerPostUserName = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            Log.d("mHandlerPostPhone", msg.obj.toString());
            try {
                JSONObject ret = new JSONObject((String) msg.obj);
                JSONArray retA = ret.getJSONArray("data");
                String retCount = retA.getJSONObject(0).getString("count(1)");
                Log.d("mHandlerPostPhone", retCount);
                if (!retCount.equals("0")) {
                    txtUserName.setError("This username is not available. Please try another.");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };
    String profileString = "";
    private EditText txtFullName = null, txtUserName = null,
            txtPassword = null, txtConfirmPasswd = null,
            txtEmail = null, txtDob = null, txtPhone = null,
            txtAns1 = null, txtAns2 = null, txtHomeTown = null, txtNickName = null, txtAddress = null;
    private ImageView profileImage;
    private Intent home = null;
    private SharedPreferences appData;
    private ProgressDialog progressBar = null;
    Handler mHandlerPostSave = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try {
                JSONObject ret = new JSONObject((String) msg.obj);
                toastShow(ret.getString("msg"));
                if (ret.getString("status").equals("1")) {
                    home.putExtra("username", txtFullName.getText().toString());
                    home.putExtra("profilePhoto", profileString);
                    SharedPreferences.Editor editor = appData.edit();
                    editor.putBoolean("login_status", true);
                    editor.commit();
                    startActivity(home);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressBar.dismiss();
        }
    };
    private Spinner spnQues1 = null, spnQues2 = null, spnCountry = null, spnState = null, spnCity = null;
    private String countryid = null, ques1id = null, ques2id = null, stateid = null, cityid = null;
    private ArrayList<IDValue> quesList = new ArrayList<IDValue>();
    private Context mContext;
    private Handler mHandlerGet = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try {
                JSONObject country_ques = new JSONObject((String) msg.obj);
                JSONArray country = country_ques.getJSONArray("country");
                addSpinnerCountryList(country);
                JSONArray questions = country_ques.getJSONArray("questions");
                initializeSpinnerQuestionsLists(questions);
                Log.d("country", country.getString(0));
                Log.d("question", questions.getString(0));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void updateCitySpinnerList(JSONArray cityArray) {
        ArrayList<IDValue> cityList = new ArrayList<IDValue>();
        try {
            if (cityArray == null)
                cityArray = new JSONArray().put(new JSONObject("{\"cityid\":\"0\",\"cityname\":\"NO VALUE!\""));
            for (int i = 0; i < cityArray.length(); i++) {

                JSONObject state = cityArray.getJSONObject(i);
                cityList.add(i, new IDValue(state.getString("cityid"), state.getString("cityname")));
            }

        } catch (JSONException e) {
            cityList.add(0, new IDValue("0", "NO VALUE!"));
            e.printStackTrace();
        }
        IDValueArrayAdapter cityAdapter = new IDValueArrayAdapter(this, android.R.layout.simple_spinner_item, cityList);
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnCity.setAdapter(cityAdapter);
        spnCity.setOnItemSelectedListener(new SpinnerActivity());
    }

    private void updateStateSpinnerList(JSONArray stateArray) {
        ArrayList<IDValue> stateList = new ArrayList<IDValue>();
        try {
            if (stateArray == null)
                stateArray = new JSONArray().put(new JSONObject("{\"stateid\":\"0\",\"statename\":\"NO VALUE!\""));
            for (int i = 0; i < stateArray.length(); i++) {

                JSONObject state = stateArray.getJSONObject(i);
                stateList.add(i, new IDValue(state.getString("stateid"), state.getString("statename")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            stateList.add(0, new IDValue("0", "NO VALUE!"));
        }
        IDValueArrayAdapter stateAdapter = new IDValueArrayAdapter(this, android.R.layout.simple_spinner_item, stateList);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnState.setAdapter(stateAdapter);
        spnState.setOnItemSelectedListener(new SpinnerActivity());

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PROFILE && resultCode == 1 && data != null) {
            profileString = data.getStringExtra("croppedPhoto");
            Log.d("Registration profile", profileString);
            byte[] imageByteCode = Base64.decode(profileString, 0);
            Bitmap bitmap = BitmapFactory.decodeByteArray(imageByteCode, 0, imageByteCode.length);
            profileImage.setImageBitmap(bitmap);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlatUI.initDefaultValues(this);

        // Default theme should be set before content view is added
        FlatUI.setDefaultTheme(APP_THEME);
        mContext = this;
        setContentView(R.layout.activity_registration);
        // Getting action bar background and applying it
        getSupportActionBar().setBackgroundDrawable(FlatUI.getActionBarDrawable(this, APP_THEME, false));
        getSupportActionBar().setBackgroundDrawable(FlatUI.getActionBarDrawable(this, APP_THEME, false));
        appData = PreferenceManager.getDefaultSharedPreferences(this);
        home = new Intent(this, MemberHomeActivity.class);

        txtFullName = (EditText) findViewById(R.id.txt_fullname);
        txtUserName = (EditText) findViewById(R.id.txt_username);
        txtPassword = (EditText) findViewById(R.id.txt_password);
        txtConfirmPasswd = (EditText) findViewById(R.id.txt_confirm_passwd);
        txtDob = (EditText) findViewById(R.id.txt_dob);
        txtEmail = (EditText) findViewById(R.id.txt_email);
        txtPhone = (EditText) findViewById(R.id.txt_phone);
        txtAns1 = (EditText) findViewById(R.id.txt_ans1);
        txtAns2 = (EditText) findViewById(R.id.txt_ans2);
        txtHomeTown = (EditText) findViewById(R.id.txt_hometown);
        txtNickName = (EditText) findViewById(R.id.txt_nickname);
        txtAddress = (EditText) findViewById(R.id.txt_address);
        spnQues1 = (Spinner) findViewById(R.id.spn_ques1);
        spnQues2 = (Spinner) findViewById(R.id.spn_ques2);
        spnCountry = (Spinner) findViewById(R.id.spn_country);
        spnState = (Spinner) findViewById(R.id.spn_state);
        spnCity = (Spinner) findViewById(R.id.spn_city);
        profileImage = (ImageView) findViewById(R.id.profile_image);
        final Intent profileCrop = new Intent(this, ProfileCropActivity.class);
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(profileCrop, REQUEST_PROFILE);
            }
        });
        loadCountryQuestion();
        spnCountry.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                loadState("1");
            }
        });
        spnState.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                loadCity("1", "1");
            }
        });
        final Intent nextPage = new Intent(this, RegistrationActivity2.class);
        Button btnSignUp = (Button) findViewById(R.id.btn_signup_reg);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utilities.isOnline(mContext)) {
                    return;
                }
                boolean complete;
                complete = checkRequiredFilled(txtFullName) && txtFullName.getError() == null;
                complete = complete && checkRequiredFilled(txtPassword) && txtPassword.getError() == null;
                complete = complete && checkRequiredFilled(txtConfirmPasswd) && txtConfirmPasswd.getError() == null;
                boolean completeUserId = (txtUserName.length() > 0) && txtUserName.getError() == null;
                completeUserId = completeUserId || (txtEmail.length() > 0) && txtEmail.getError() == null;
                completeUserId = completeUserId || (txtPhone.length() > 0) && txtPhone.getError() == null;
                complete = complete && completeUserId;

                complete = complete && checkRequiredFilled(txtAns1) && txtAns1.getError() == null;
                complete = complete && checkRequiredFilled(txtAns2) && txtAns2.getError() == null;
                complete = complete && ques1id != null && ques2id != null && countryid != null && stateid != null && cityid != null;
                complete = complete && !(ques1id.equals("0") || ques2id.equals("0") || countryid.equals("0") || stateid.equals("0") || cityid.equals("0"));
                if (!completeUserId) {
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(mContext);
                    dlgAlert.setMessage("One of UserName, Email and Phone is required and must be unique. Please fill it.");
                    dlgAlert.setTitle("Login ID required.");
                    dlgAlert.setPositiveButton("OK", null);
                    //dlgAlert.setCancelable(true);
                    dlgAlert.create().show();
                    txtUserName.requestFocus();
                }
                if (!complete) {
                    toastShow("Please check again! Something might wrong.");
                    return;
                }


                saveRegistration();
                /*
                UserInfo userInfo1 = new UserInfo(txtFullName.getText().toString(), txtUserName.getText().toString(), txtPassword.getText().toString(), txtConfirmPasswd.getText().toString(), txtEmail.getText().toString(), txtDob.getText().toString(), txtPhone.getText().toString());
                nextPage.putExtra("userinfo1", userInfo1);
                startActivity(nextPage);*/
            }
        });
        addRequired(txtFullName);
        addRequired(txtPassword);
        addRequired(txtConfirmPasswd);
        //addRequired(txtUserName);
        //addRequired(txtEmail);
        //addRequired(txtPhone);
        addRequired(txtAns1);
        addRequired(txtAns2);
        txtPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && txtPhone.length() > 0) {
                    if (txtPhone.length() != 10) {
                        txtPhone.setError("Invalid Phone number. Please add mobile phone number");
                    } else {
                        checkPhoneUsed(txtPhone.getText().toString());
                    }
                }
            }
        });
        txtPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String special = "[!@#$%^&*()_+]";
                    String digit = "[0-9]+";
                    if (txtPassword.getText().length() < 6) {
                        txtPassword.setError("Password must be at least 6 characters.");
                    } else if (!Pattern.compile(special).matcher(txtPassword.getText()).find()) {
                        txtPassword.setError("Password must contain special characters.");
                    } else if (!Pattern.compile(digit).matcher(txtPassword.getText()).find()) {
                        txtPassword.setError("Password must contain at least one digit.");
                    } else if (txtPassword.getText().toString().contains(txtUserName.getText()) && txtUserName.length() > 0) {
                        txtPassword.setError("Password should not contain username.");
                    } else {
                        String[] names = txtFullName.getText().toString().split(" ");
                        for (int i = 0; i < names.length; i++) {
                            if (txtPassword.getText().toString().toLowerCase().contains((names[i]).toLowerCase()))
                                txtPassword.setError("Password should not contain your name.");
                        }
                    }
                }

            }

        });
        txtConfirmPasswd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus && !txtConfirmPasswd.getText().toString().equals(txtPassword.getText().toString())) {
                    txtConfirmPasswd.setError("Password and confirm password mismatch.");
                }
            }
        });
        txtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                String email = "^[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$";
                if (!hasFocus && txtEmail.length() > 0) {
                    if (!Pattern.compile(email).matcher(txtEmail.getText()).find()) {
                        txtEmail.setError("Invalid Email.");
                    } else {
                        checkEmailUsed(txtEmail.getText().toString());
                    }
                }
            }
        });
        txtUserName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && txtUserName.length() > 0) {
                    if (txtUserName.length() < 6) {
                        txtUserName.setError("Need at least 6 characters.");
                    } else {
                        checkUserNameUsed(txtUserName.getText().toString());
                    }
                }
            }
        });
        txtUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String userRegex = "[\\s]+";
                if (Pattern.compile(userRegex).matcher(s).find()) {
                    txtUserName.setError("Invalid Username. Space should remove.");
                }
            }
        });
    }

    private void saveRegistration() {
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Registering in ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
        RestService saveUserPost = new RestService(mHandlerPostSave, mContext, "http://23.92.53.174/testws/signup12.php", RestService.POST); //Create new rest service for post
        saveUserPost.addParam("SignMode", "1");
        saveUserPost.addParam("p_UserName", txtUserName.getText().toString());
        saveUserPost.addParam("p_EmailAddress", txtEmail.getText().toString());
        saveUserPost.addParam("p_MobileNumber", txtPhone.getText().toString());
        saveUserPost.addParam("p_Country", countryid);
        saveUserPost.addParam("p_State", stateid);
        saveUserPost.addParam("p_City", cityid);
        saveUserPost.addParam("p_HomeTown", txtHomeTown.getText().toString());
        saveUserPost.addParam("p_FullName", txtFullName.getText().toString());
        saveUserPost.addParam("p_DOB", txtDob.getText().toString());
        saveUserPost.addParam("p_NickName", txtNickName.getText().toString());
        saveUserPost.addParam("p_BillingAddress", txtAddress.getText().toString());
        saveUserPost.addParam("p_ProfilePicture", profileString);
        saveUserPost.addParam("p_Plain_Password_text", txtPassword.getText().toString());
        saveUserPost.addParam("p_EncryptionPasswrod", Utilities.stringToMD5(txtPassword.getText().toString()));
        saveUserPost.addParam("p_qus1", ques1id);
        saveUserPost.addParam("p_ans1", txtAns1.getText().toString());
        saveUserPost.addParam("p_qus2", ques2id);
        saveUserPost.addParam("p_ans2", txtAns2.getText().toString());
        saveUserPost.execute();


    }

    private void checkUserNameUsed(String username) {
        RestService getState = new RestService(mHandlerPostUserName, mContext, "http://23.92.53.174/testws/getdroplist.php", RestService.POST);
        getState.setEntity("{\"type\":3,\"username\":\"" + username + "\"}");
        getState.execute();
    }

    private void checkEmailUsed(String email) {
        RestService getState = new RestService(mHandlerPostEmail, mContext, "http://23.92.53.174/testws/getdroplist.php", RestService.POST);
        getState.setEntity("{\"type\":4,\"email\":\"" + email + "\"}");
        getState.execute();
    }

    private void checkPhoneUsed(String phone) {
        RestService getState = new RestService(mHandlerPostPhone, mContext, "http://23.92.53.174/testws/getdroplist.php", RestService.POST);
        getState.setEntity("{\"type\":5,\"mobilenumber\":" + phone + "}");
        getState.execute();
    }

    private void loadCity(String countryId, String stateId) {
        String entity = "{\"type\":2,\"country\":" + countryId + ",\"state\":" + stateId + "}";
        Log.d("loadCity", entity);
        RestService getState = new RestService(mHandlerPostCity, mContext, "http://23.92.53.174/testws/getdroplist.php", RestService.POST);
        getState.setEntity(entity);
        getState.execute();
    }

    private void loadState(String countryId) {
        String entity = "{\"type\":1,\"country\":" + countryId + "}";
        Log.d("loadState", entity);
        RestService getState = new RestService(mHandlerPostState, mContext, "http://23.92.53.174/testws/getdroplist.php", RestService.POST);
        getState.setEntity(entity);
        getState.execute();
    }

    private void loadCountryQuestion() {

        RestService getCountryQuestion = new RestService(mHandlerGet, this, "http://23.92.53.174/testws/SignupBefore.php", RestService.GET);
        getCountryQuestion.execute();
    }

    private void initializeSpinnerQuestionsLists(JSONArray questions) {
        ArrayList<IDValue> q1List = new ArrayList<IDValue>();
        ArrayList<IDValue> q2List = new ArrayList<IDValue>();
        try {
            String quesid;
            String questionsName;
            JSONObject item = questions.getJSONObject(0);
            quesid = item.getString("questionsid");
            questionsName = item.getString("questionsname");
            q1List.add(0, new IDValue(quesid, questionsName));
            quesList.add(0, new IDValue(quesid, questionsName));
            for (int i = 1; i < questions.length(); i++) {
                item = questions.getJSONObject(i);
                quesid = item.getString("questionsid");
                questionsName = item.getString("questionsname");
                q1List.add(i, new IDValue(quesid, questionsName));
                q2List.add(i - 1, new IDValue(quesid, questionsName));
                quesList.add(i, new IDValue(quesid, questionsName)); // store questions for further use
            }
        } catch (JSONException e) {
            e.printStackTrace();
            q1List.add(0, new IDValue("0", "NO VALUE!"));
            q2List.add(0, new IDValue("0", "NO VALUE!"));
        }
        IDValueArrayAdapter question1Adapter = new IDValueArrayAdapter(this, android.R.layout.simple_spinner_item, q1List);
        question1Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnQues1.setAdapter(question1Adapter);
        spnQues1.setOnItemSelectedListener(new SpinnerActivity());

        IDValueArrayAdapter question2Adapter = new IDValueArrayAdapter(this, android.R.layout.simple_spinner_item, q2List);
        question2Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnQues2.setAdapter(question2Adapter);
        spnQues2.setOnItemSelectedListener(new SpinnerActivity());
    }

    private void addSpinnerCountryList(JSONArray country_ques) {
        ArrayList<IDValue> countryList = new ArrayList<IDValue>();
        try {
            for (int i = 0; i < country_ques.length(); i++) {

                JSONObject item = country_ques.getJSONObject(i);
                IDValue country = new IDValue(item.getString("countryid"), item.getString("countryname"));
                countryList.add(i, country);

            }
            IDValueArrayAdapter countrySpinnerAdapter = new IDValueArrayAdapter(this, android.R.layout.simple_spinner_item, countryList);
            countrySpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnCountry.setAdapter(countrySpinnerAdapter);
            spnCountry.setOnItemSelectedListener(new SpinnerActivity());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void addRequired(final EditText edittext) {

        edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() <= 0) {
                    edittext.setError("It is required");
                }
            }
        });
    }

    private boolean checkRequiredFilled(EditText editText) {
        if (editText.length() <= 0) {
            editText.setError("It is required.");
            return false;
        }
        return true;
    }

    private void updateQues2List(String ques1id) {
        ArrayList<IDValue> itemq2list = new ArrayList<IDValue>();
        int j = 0;
        for (int i = 0; i < quesList.size(); i++) {
            if (quesList.get(i).getId().equals(ques1id)) {
                continue;
            }
            itemq2list.add(j, quesList.get(i));
            j++;
        }

        IDValueArrayAdapter question2Adapter = new IDValueArrayAdapter(this, android.R.layout.simple_spinner_item, itemq2list);
        question2Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnQues2.setAdapter(question2Adapter);
        spnQues2.setOnItemSelectedListener(new SpinnerActivity());
    }

    private void toastShow(String msg) {
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        toast.show();
    }

    public class SpinnerActivity extends Activity implements AdapterView.OnItemSelectedListener {


        public void onItemSelected(AdapterView<?> parent, View view,
                                   int pos, long id) {
            // An item was selected. You can retrieve the selected item using
            // parent.getItemAtPosition(pos)
            if (parent.equals(spnQues1)) {
                ques1id = "" + id;
                updateQues2List(ques1id);
            }
            if (parent.equals(spnQues2)) {
                ques2id = "" + id;
                if (ques2id.equals(ques1id)) {
                    updateQues2List(ques1id);
                }
            }
            if (parent.equals(spnCountry)) {
                countryid = "" + id;
                loadState(countryid);

            }
            if (parent.equals(spnState)) {
                stateid = "" + id;
                loadCity(countryid, stateid);
            }
            if (parent.equals(spnCity)) {
                cityid = "" + id;
            }

        }

        public void onNothingSelected(AdapterView<?> parent) {
            // Another interface callback
        }
    }

}
