package com.sanlin.swarmlogin;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.cengalabs.flatui.FlatUI;

public class MemberHomeActivity extends ActionBarActivity {
    private final int APP_THEME = R.array.orange;
    private ImageView profileImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlatUI.initDefaultValues(this);

        // Default theme should be set before content view is added
        FlatUI.setDefaultTheme(APP_THEME);
        setContentView(R.layout.activity_member_home);
        // Getting action bar background and applying it
        getSupportActionBar().setBackgroundDrawable(FlatUI.getActionBarDrawable(this, APP_THEME, false));
        getSupportActionBar().setBackgroundDrawable(FlatUI.getActionBarDrawable(this, APP_THEME, false));

        String username=(String)getIntent().getStringExtra("username");
        TextView tv_hello=(TextView)findViewById(R.id.tv_hello);
        tv_hello.setText(tv_hello.getText()+", "+username);
        String profileString=(String)getIntent().getStringExtra("profilePhoto");
        if(profileString!=null){
            profileImage = (ImageView)findViewById(R.id.home_profile);
            byte[] imageByteCode = Base64.decode(profileString, 0);
            Bitmap bitmap = BitmapFactory.decodeByteArray(imageByteCode, 0, imageByteCode.length);
            profileImage.setImageBitmap(bitmap);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_member_home, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
