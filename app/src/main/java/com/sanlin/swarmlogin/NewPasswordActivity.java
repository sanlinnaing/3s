package com.sanlin.swarmlogin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cengalabs.flatui.FlatUI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

import lu.thinds.android.RestService;

public class NewPasswordActivity extends ActionBarActivity {
    private final int APP_THEME = R.array.orange;
    private EditText txtRstPasswd, txtRstCfrmPasswd;
    private Button btnReset;
    private ProgressDialog progressBar;
    private Intent login;
    Handler setNewPasswdHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try {
                boolean correct = true;
                JSONObject ret = new JSONObject((String) msg.obj);
                showToast(msg.obj.toString());
                if (ret.getString("st").equals("1")) {
                    progressBar.dismiss();
                    startActivity(login);
                }
                showToast(ret.getString("msg"));


            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressBar.dismiss();
        }
    };
    private String mode, loginId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlatUI.initDefaultValues(this);

        // Default theme should be set before content view is added
        FlatUI.setDefaultTheme(APP_THEME);
        setContentView(R.layout.activity_new_password);
        getSupportActionBar().setBackgroundDrawable(FlatUI.getActionBarDrawable(this, APP_THEME, false));
        getSupportActionBar().setBackgroundDrawable(FlatUI.getActionBarDrawable(this, APP_THEME, false));

        initialize();
        validatation();
        addListener();

    }

    private void addListener() {
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtRstCfrmPasswd.length() < 0)
                    txtRstCfrmPasswd.setError("Please confirm password.");
                else if (txtRstPasswd.length() < 0)
                    txtRstPasswd.setError("Please enter password.");
                else if (!(txtRstCfrmPasswd.getText().toString().equals(txtRstPasswd.getText().toString())))
                    txtRstCfrmPasswd.setError("Please reconfirm again. Passwords not match.");
                else
                    setNewPassword();
            }
        });
    }

    private void setNewPassword() {
        String passwd = txtRstPasswd.getText().toString();
        String md5Passwd = Utilities.stringToMD5(passwd);
        String entity = "{\"type\":2,\"mode\":" + mode + ",\"username\":\"" + loginId + "\",\"newpassword\":\"" + md5Passwd + "\"}";
        Log.d("setNewPasswd", entity);
        RestService setNewPasswdPost = new RestService(setNewPasswdHandler, this, "http://23.92.53.174/testws/management.php", RestService.POST);
        setNewPasswdPost.setEntity(entity);
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Setting new password ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.setCancelable(false);
        progressBar.show();
        setNewPasswdPost.execute();
    }

    private void validatation() {
        txtRstPasswd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String special = "[!@#$%^&*()_+]";
                    String digit = "[0-9]+";
                    if (txtRstPasswd.getText().length() < 6) {
                        txtRstPasswd.setError("Password must be at least 6 characters.");
                    } else if (!Pattern.compile(special).matcher(txtRstPasswd.getText()).find()) {
                        txtRstPasswd.setError("Password must contain special characters.");
                    } else if (!Pattern.compile(digit).matcher(txtRstPasswd.getText()).find()) {
                        txtRstPasswd.setError("Password must contain at least one digit.");
                    }
                }
            }
        });
        txtRstCfrmPasswd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && txtRstCfrmPasswd.length() < 1) {
                    txtRstCfrmPasswd.setError("It is required.");
                }
            }
        });

    }

    private void initialize() {
        txtRstPasswd = (EditText) findViewById(R.id.txt_rst_passwd);
        txtRstCfrmPasswd = (EditText) findViewById(R.id.txt_rst_cfrm_psswd);
        btnReset = (Button) findViewById(R.id.btn_rst_reset);
        login = new Intent(this, LoginActivity.class);
        mode = getIntent().getStringExtra("mode");
        loginId = getIntent().getStringExtra("loginId");
    }

    private void showToast(String msg) {
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        toast.show();
    }
}
