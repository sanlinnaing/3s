package com.sanlin.swarmlogin;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.cengalabs.flatui.FlatUI;

public class HomeActivity extends ActionBarActivity {
    MenuItem logmenuitem = null;
    private SharedPreferences appData;
    private final int APP_THEME = R.array.orange;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // converts the default values to dp to be compatible with different screen sizes
        FlatUI.initDefaultValues(this);

        // Default theme should be set before content view is added
        FlatUI.setDefaultTheme(APP_THEME);

        setContentView(R.layout.activity_home);

        // Getting action bar background and applying it
        getSupportActionBar().setBackgroundDrawable(FlatUI.getActionBarDrawable(this, APP_THEME, false));
        getSupportActionBar().setBackgroundDrawable(FlatUI.getActionBarDrawable(this,APP_THEME, false));
        appData = PreferenceManager.getDefaultSharedPreferences(this);
        Log.d("Key :: :"," key = : ; + = "+Utilities.printKeyHash(this));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        if (appData.getBoolean("login_status", false)) {
            logmenuitem = menu.findItem(R.id.action_settings);
            logmenuitem.setTitle("Log out");

            ((TextView) findViewById(R.id.home_greeting)).setText("Welcome Back");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            if (appData.getBoolean("login_status", false)) {
                item.setTitle("Log in");
                ((TextView) findViewById(R.id.home_greeting)).setText("Please Log in");
                SharedPreferences.Editor editor = appData.edit();
                editor.putBoolean("login_status", false);
                editor.commit();

            } else if(Utilities.isOnline(this)) {
                Intent login = new Intent(this, LoginActivity.class);
                startActivity(login);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (appData.getBoolean("login_status", false)) {
            if (logmenuitem != null)
                logmenuitem.setTitle("Log out");
            ((TextView) findViewById(R.id.home_greeting)).setText("Welcome Back");
        }
    }
}
