package com.sanlin.swarmlogin;

/**
 * Created by San Lin Naing on 8/26/2015.
 */


public class IDValue {
    private String id;
    private String value;

    public IDValue(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
