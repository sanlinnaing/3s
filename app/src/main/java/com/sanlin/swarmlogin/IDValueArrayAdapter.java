package com.sanlin.swarmlogin;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by San Lin Naing on 8/26/2015.
 */
public class IDValueArrayAdapter extends ArrayAdapter<IDValue> {
    private Context mContext;
    private ArrayList<IDValue> values;

    public IDValueArrayAdapter(Context context, int resource, ArrayList<IDValue> objects) {
        super(context, resource, objects);
        mContext=context;
        values=objects;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public IDValue getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(values.get(position).getId());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)   getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView tvLabel =(TextView)inflater.inflate(R.layout.text,null);
        tvLabel.setText(values.get(position).getValue());
        return tvLabel;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)   getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView tvLabel =(TextView)inflater.inflate(R.layout.text,null);

        tvLabel.setText(values.get(position).getValue());
        return tvLabel;
    }
}
